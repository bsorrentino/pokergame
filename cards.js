var cards = []; // array containing cards


function shuffle() {
    
    cards = [];
    
    // iterate over seeds
    for( var _seed = 1; _seed <= 4 ; ++_seed ) {

         // iterate over value
        for( var _value = 1 ; _value <= 13 ; ++_value ) {

            cards.push( { seed:_seed, value:_value } );
            

        }
    }
    checkCards();
    coveredCards();
}


function extractCardAndRemoveFromCards() {
    return cards.splice(Math.floor(Math.random()*cards.length), 1)[0];
}



function getSeedInfo( _card ) {
    
   if( _card.seed == 1 ) {
        return { symbol:"c", color:"red"};
   }
    else if( _card.seed == 2 ) {
        return { symbol:"q", color:"red"};
    }
    else if( _card.seed == 3 ) {
        return { symbol:"f", color:"black"};
    }
    else if( _card.seed == 4 ) {
        return { symbol:"p", color:"black"};
    }
    
    return { symbol:"?", color:''};
}

function getValueSymbol( _card ) {
    var v = _card.value;
    
   if( v == 11 ) {
            return "J";
   }
    else if( v == 12 ) {
        return "Q";
    }
    else if( v == 13 ) {
        return "K";
    }
    else if( v == 1 ) {
        return "A";
    }
    
    return _card.value;
}

var hand = [];

function delay() {
    console.log( "delay", Date.now() );
}

function showCard( card, i ) {
        var seedInfo = getSeedInfo(card);
        
        var e = document.getElementById("c" + i );
        
        e.innerHTML = getValueSymbol(card) + 
                    " " + 
                  seedInfo.symbol;
        e.style.color = seedInfo.color;
        
 
}

function showCars() {
}

function play() {


    for( var i = 0 ; i < 5 ; ++i ) {
    
        var card = extractCardAndRemoveFromCards();
        
        console.log( "card", card );
    
        hand.push( card );
        
        showCard( card, i );      
        checkCards();
        // ShowbtnShuffle()
    }
    
    
}

/* function playLazy() {
    playLazy.called = false; 
    var i = 0;
    
    var interval = window.setInterval( function() {
        console.log( "interval", i ) ;
        
        if( i == 5 ) {
            window.clearInterval(interval);
            return;
        }
        
         var card = extractCardAndRemoveFromCards();
        
        console.log( "card", card );
    
        hand.push( card );
        
        showCard( card, i++ );  
        document.getElementById("remainingCards").innerHTML = remainingCards() + " carte rimanenti"
        console.log(remainingCards()+ "carte rimanenti")
        
    }, 200 );
    endedCards();
    //  HidebtnShuffle();
    ShowbtnShuffle();
    
}*/
/*
function HidebtnShuffle(){
    if (playLazy.called = true) {
        document.getElementById("btnShuffle").style.visibility = "collapse";
        playLazy.called = false;
    } 
}
*/
/*function ShowbtnShuffle(){
    if (playLazy.called = true) {
        document.getElementById("btnShuffle").disabled = false;
        playLazy.called = false;
    } 
    if (playLazy.called = false) {
        document.getElementById("btnShuffle").disabled = true;
        playLazy.called = true;
    }
} */
function checkCards() {
    var remainingCards = cards.length;
    document.getElementById("remainingCards").innerHTML = remainingCards + " carte rimanenti";
    if (remainingCards == 0) {
        alert("carte finite");
    }
}

function coveredCards() {
        document.getElementById("c0").innerHTML = "X";
        document.getElementById("c0").style.color = "black";
        document.getElementById("c1").innerHTML = "X";
        document.getElementById("c1").style.color = "black";
        document.getElementById("c2").innerHTML = "X";
        document.getElementById("c2").style.color = "black";
        document.getElementById("c3").innerHTML = "X";
        document.getElementById("c3").style.color = "black";
        document.getElementById("c4").innerHTML = "X";    
        document.getElementById("c4").style.color = "black";
}

function changeCardsCheckbox() {
    for (var i = 0; i <= 4; i++) {
    var ckb = document.getElementById("ckb" + i);
        if (ckb.checked == true) {
            var card = extractCardAndRemoveFromCards();
            console.log(card);
            card.style = getSeedInfo.style;
            document.getElementById("c" + i).innerHTML = getValueSymbol(card) + " " + getSeedInfo(card).symbol;
            document.getElementById("c" + i).style.color = getSeedInfo(card).color;
            
        }
        ckb.checked = false;
    }
}

